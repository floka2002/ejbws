package ru.floka;

import java.math.BigDecimal;

/**
 *
 * @author floka-ab
 */
    class Report{

        private BigDecimal Code;
        public BigDecimal getCode() {
            return Code;
        }
        public void setCode(BigDecimal Code) {
            this.Code = Code;
        }
        private String Description;
        public String getDescription() {
            return Description;
        }
        public void setDescription(String Description) {
            this.Description = Description;
        }

        private int count;
        public int getCount() {
            return count;
        }
        public void setCount(int count) {
            this.count = count;
        }
    }