/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.floka.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author floka-ab
 */
@Entity
@Table(name = "USLUGA", catalog = "", schema = "FLOKA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usluga.findAll", query = "SELECT u FROM Usluga u ORDER BY u.code"),
    @NamedQuery(name = "Usluga.findByCode", query = "SELECT u FROM Usluga u WHERE u.code = :code"),
    @NamedQuery(name = "Usluga.findByDescription", query = "SELECT u FROM Usluga u WHERE u.description = :description")})
public class Usluga implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    //Генерации id из последовательности по заданию не требуется
    //@GeneratedValue(generator = "UslugaSeq")
    //@SequenceGenerator(name = "UslugaSeq", sequenceName = "USLUGA_SEQ", allocationSize = 1)
    @Column(name = "CODE", nullable = false, precision = 0, scale = -127)
    private BigDecimal code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPTION", nullable = false, length = 255)
    private String description;
    @JoinTable(name = "ABON_USLUGA", joinColumns = {
        @JoinColumn(name = "CODE", referencedColumnName = "CODE", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "NLS", referencedColumnName = "NLS", nullable = false)})
    @ManyToMany
    private Collection<Abon> abonCollection;

    public Usluga() {
    }

    public Usluga(BigDecimal code) {
        this.code = code;
    }

    public Usluga(BigDecimal code, String description) {
        this.code = code;
        this.description = description;
    }

    public BigDecimal getCode() {
        return code;
    }

    public void setCode(BigDecimal code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Abon> getAbonCollection() {
        return abonCollection;
    }

    public void setAbonCollection(Collection<Abon> abonCollection) {
        this.abonCollection = abonCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usluga)) {
            return false;
        }
        Usluga other = (Usluga) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.floka.entity.Usluga[ code=" + code + " ]";
    }
    
}
