/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.floka.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author floka-ab
 */
@Entity
@Table(name = "ABON", catalog = "", schema = "FLOKA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Abon.findAbon", query = "SELECT a FROM Abon a WHERE a.nls = :nls and a.lastName = :lastName and a.firstName = :firstName and a.secondName = :secondName"),
    @NamedQuery(name = "Abon.findAll", query = "SELECT a FROM Abon a"),
    @NamedQuery(name = "Abon.findByNls", query = "SELECT a FROM Abon a WHERE a.nls = :nls"),
    @NamedQuery(name = "Abon.findByLastName", query = "SELECT a FROM Abon a WHERE a.lastName = :lastName"),
    @NamedQuery(name = "Abon.findByFirstName", query = "SELECT a FROM Abon a WHERE a.firstName = :firstName"),
    @NamedQuery(name = "Abon.findBySecondName", query = "SELECT a FROM Abon a WHERE a.secondName = :secondName")})
public class Abon implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    //Генерации id из последовательности по заданию не требуется
    //@GeneratedValue(generator = "AbonSeq")
    //@SequenceGenerator(name = "AbonSeq", sequenceName = "ABON_SEQ", allocationSize = 1)
    @Column(name = "NLS", nullable = false, precision = 0, scale = -127)
    private BigDecimal nls;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "LAST_NAME", nullable = false, length = 255)
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "FIRST_NAME", nullable = false, length = 255)
    private String firstName;
    @Size(max = 255)
    @Column(name = "SECOND_NAME", length = 255)
    private String secondName;
    @ManyToMany(mappedBy = "abonCollection")
    private Collection<Usluga> uslugaCollection;

    public Abon() {
    }

    public Abon(BigDecimal nls) {
        this.nls = nls;
    }

    public Abon(BigDecimal nls, String lastName, String firstName) {
        this.nls = nls;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public BigDecimal getNls() {
        return nls;
    }

    public void setNls(BigDecimal nls) {
        this.nls = nls;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @XmlTransient
    public Collection<Usluga> getUslugaCollection() {
        return uslugaCollection;
    }

    public void setUslugaCollection(Collection<Usluga> uslugaCollection) {
        this.uslugaCollection = uslugaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nls != null ? nls.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Abon)) {
            return false;
        }
        Abon other = (Abon) object;
        if ((this.nls == null && other.nls != null) || (this.nls != null && !this.nls.equals(other.nls))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.floka.entity.Abon[ nls=" + nls + " ]";
    }
    
}
