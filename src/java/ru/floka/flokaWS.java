package ru.floka;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import ru.floka.entity.Abon;
import ru.floka.entity.Usluga;

/**
 *
 * @author floka-ab
 */
@WebService(serviceName = "flokaWS")
@Stateless()
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class flokaWS {

    @PersistenceContext(unitName = "EJBWSPU")
    private EntityManager em;
    @Resource
    private UserTransaction utx;
    @Resource
    private SessionContext sessionContext;

    /**
     * Операция веб-службы Поиск карточки абонента
     * ищет по лицевому номеру, если он пустой то like по всем полям запроса.
     */
    @WebMethod(operationName = "findAbon")
    public List<ru.floka.entity.Abon> findAbon(@WebParam(name = "parameter") final Abon abon_in) {
        if (abon_in.getNls() != null) {
            Query q = em.createNamedQuery("Abon.findByNls");
            q.setParameter("nls", abon_in.getNls());
            List result = q.getResultList();
            return result;
        }
        Query q = em.createNativeQuery("select * from abon where first_name like ? and second_name like ? and last_name like ? order by last_name, first_name", Abon.class);
        q.setParameter(1, abon_in.getFirstName() + "%");
        q.setParameter(2, abon_in.getSecondName() + "%");
        q.setParameter(3, abon_in.getLastName() + "%");
        List result = q.getResultList();
        return result;
    }

    /**
     * Операция веб-службы Заведение карточки абонента
     */
    @WebMethod(operationName = "addAbon")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int addAbon(@WebParam(name = "parameter") final Abon abon_in) {
        try {
            em.persist(abon_in);
            return 0;
        } catch (Exception ex) {
            Logger.getLogger(flokaWS.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }

    /**
     * Операция веб-службы Редактирование атрибутов карточки абонента
     */
    @WebMethod(operationName = "editAbon")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int editAbon(@WebParam(name = "parameter") final Abon abon_in) {
        try {
            em.merge(abon_in);
            return 0;
        } catch (Exception ex) {
            Logger.getLogger(flokaWS.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }

    /**
     * Операция веб-службы Загрузка справочника услуг
     */
    @WebMethod(operationName = "createUslugs")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int createUslugs(@WebParam(name = "parameter") final String strE_in) {
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            String str_in = new String(decoder.decode(strE_in),StandardCharsets.UTF_8);
            String[] str = str_in.split("\n");
            em.createNativeQuery("truncate table usluga").executeUpdate();
            for (String str1 : str) {
                String[] p = str1.split(",");
                Usluga u = new Usluga();
                u.setCode(BigDecimal.valueOf(Long.parseLong(p[0])));
                u.setDescription(p[1]);
                em.persist(u);
            }
            return 0;
        } catch (Exception ex) {
            Logger.getLogger(flokaWS.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }
    
    /**
     * Операция веб-службы Подключение услуги
     */
    @WebMethod(operationName = "addUsluga2Abon")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int addUsluga2Abon(@WebParam(name = "Nls") final long Nls, @WebParam(name = "Code") final long Code) {
        try {
            Query q = em.createNamedQuery("Abon.findByNls");
            q.setParameter("nls",Nls);
            Abon a = (Abon)q.getSingleResult();
            //Collection<Usluga> c = a.getUslugaCollection();
            
            Query q1 = em.createNamedQuery("Usluga.findByCode");
            q1.setParameter("code",Code);
            Usluga u = (Usluga)q1.getSingleResult();
            Collection<Abon> ca = u.getAbonCollection();
            
            ca.add(a);
            em.persist(u);
            return 0;
        } catch (Exception ex) {
            Logger.getLogger(flokaWS.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }
    
    /**
     * Операция веб-службы Отключение услуги
     */
    @WebMethod(operationName = "deleteUsluga2Abon")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int deleteUsluga2Abon(@WebParam(name = "Nls") final long Nls, @WebParam(name = "Code") final long Code) {
        try {
            Query q = em.createNamedQuery("Abon.findByNls");
            q.setParameter("nls",Nls);
            Abon a = (Abon)q.getSingleResult();
            //Collection<Usluga> c = a.getUslugaCollection();
            
            Query q1 = em.createNamedQuery("Usluga.findByCode");
            q1.setParameter("code",Code);
            Usluga u = (Usluga)q1.getSingleResult();
            Collection<Abon> ca = u.getAbonCollection();
            
            ca.remove(a);
            em.persist(u);
            return 0;
        } catch (Exception ex) {
            Logger.getLogger(flokaWS.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }
    
    /**
     * Операция веб-службы Справочник услуг
     */
    @WebMethod(operationName = "allUslugs")
    public List<ru.floka.entity.Usluga> allUslugs() {
            Query q = em.createNamedQuery("Usluga.findAll");
            List result = q.getResultList();
            return result;
    }
    
    /**
     * Операция веб-службы Отчет по использованию услуг
     */
    @WebMethod(operationName = "reportUslugs")
    public List<Report> reportUslugs() {
            Query q = em.createNamedQuery("Usluga.findAll",Usluga.class);
            List<Usluga> result = q.getResultList();
            List<Report> ls = new ArrayList<>();
            for(Usluga u : result){
               Report r = new Report();
               r.setCode(u.getCode());
               r.setDescription(u.getDescription());
               r.setCount(u.getAbonCollection().size());
               ls.add(r);
            }
            return ls;
    }

}
